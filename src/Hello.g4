grammar Hello;

hello : 'begin survey' NL+ survey+ 'end survey' NL+ ;
survey : textbox | singlechoise ;
textbox : 'textbox' (tag?|NL) title height ;
singlechoise : 'singlechoise' (tag?|NL) title option+ ;
title : 'title' TEXT NL ;
option : 'option' TEXT NL ;
height : 'height' NUMBER NL ;
tag : '#' NUMBER NL ;
TEXT : '"' ([a-z]|[A-Z]|[0-9]|WS)* '"' ;
NUMBER : '-'? INT ;
fragment INT : '0' | [1-9] [0-9]* ;
NL : '\r'?'\n';
WS  :   [ \t\n\r]+ -> skip ;

