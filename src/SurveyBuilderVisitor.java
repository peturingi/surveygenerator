import org.antlr.v4.runtime.misc.NotNull;

import javax.xml.soap.Text;
import java.util.ArrayList;
import java.util.List;

public class SurveyBuilderVisitor extends HelloBaseVisitor<String> {

  @Override
  public String visitTextbox(@NotNull HelloParser.TextboxContext ctx) {
    final Textbox textbox = new Textbox();
    textbox.height = Integer.parseInt(ctx.height().NUMBER().getText());
    textbox.tag = ctx.tag().NUMBER().getText();
    textbox.title = stripQuotes(ctx.title().TEXT().getText());
    System.out.println(textbox.toString() +'\n');
    return ctx.getText();
  }

  @Override
  public String visitSinglechoise(@NotNull HelloParser.SinglechoiseContext ctx) {
    final List<Option> options = new ArrayList<Option>();
    for (HelloParser.OptionContext o: ctx.option())
      options.add(new Option(stripQuotes(o.TEXT().getText())));

    final Singlechoise singlechoise = new Singlechoise();
    singlechoise.tag = ctx.tag().NUMBER().getText();
    singlechoise.title = stripQuotes(ctx.title().TEXT().getText());
    singlechoise.options = options;
    System.out.println(singlechoise.toString());
    return ctx.getText();
  }

  private String stripQuotes(final String string) {
    return string.substring(1, string.length()-1);
  }

}
