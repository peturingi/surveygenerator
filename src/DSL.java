import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class DSL {
  public static void main(String[] args) {
    final FileInputStream inputStream;
    try {
      inputStream = new FileInputStream("/Users/petur/Development/DSL/src/Survey.dsl");
      final ANTLRInputStream input = new ANTLRInputStream(inputStream);
      final HelloLexer lexer = new HelloLexer(input);
      final CommonTokenStream tokens = new CommonTokenStream(lexer);
      final HelloParser parser = new HelloParser(tokens);
      final ParseTree tree = parser.hello();

      final SurveyBuilderVisitor visitor = new SurveyBuilderVisitor();
      visitor.visit(tree);

      inputStream.close();
    }
    catch (FileNotFoundException e) {
      System.err.println("File not found.");
      System.exit(1);
    }
    catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }
}
