// Comments span an entire line and are ignored.

// Survey with id 1
survey 1
  title "Elderly Care"

// Part with id 1
  part 1
    title "User Information"
    frequency ONCE

// open dk.dtu.atse.dsl.questioner.question with id 1, expects an answer of type int, optional dk.dtu.atse.dsl.questioner.question where an empty answer is allowed.
    open 1 INT OPTIONAL
      title "How old are you?"

// Closed dk.dtu.atse.dsl.questioner.question with id 2. Expects a single answer (min 1, max 1).
// 1 and 2 are option ids.
    closed 2
      title "Sex"
      min 1
      max 1
      option 1 "Male"
      option 2 "Female"

  part 2
    title "Breakfast"
    frequency DAILY

    closed 1
      title "Did you eat breakfast?"
      min 1
      max 1
      option 1 "Yes"
      option 2 "No"

    open 2 TEXT
      title "What did you eat?"

    closed 3
      title "Are you tired?"
      min 1
      max 1
      option 1 "Yes"
      option 2 "No"

    open 4 INT
      title "What is your heart rate?"

  part 3
    title "Coffee"
    frequency DAILY

    closed 1
      title "How do you like your coffee?"
      min 1
      max 5
      option 1 "with Milk"
      option 2 "with Sugar"
      option 3 "with Syrup"
      option 4 "with Cookies"
      option 5 "other"

    open 2 TEXT
    title "What else do you like with your coffee?"

  part 4
    title "Exercise"
    frequency WEEKLY

    open 1 INT
      title "For how many hours did you exercise in the passing week?"

  part 5
    title "Temperature"
    frequency DAILY

    open 1 FLOAT
      title "What is your temperature in °C ?"

  part 6
    title "Water"
    frequency DAILY

    open 1 FLOAT
      title "How many liters of water have you consumed in the last 24 hours?"



// Contingency on Survey with id 1.
contingency 1
  // x.y refers to Part_X Question_Y
  // 2.1, 3.1 are CLOSED questions. = 1 refers to option 1 in dk.dtu.atse.dsl.questioner.question number 1 in part 2.
  2.2 if 2.1 = 1      // Ask what did you eat if did eat breakfast
  3.2 if 3.1 = 5      // Ask what else you like with coffee, if you said you like something else.
  // 5.1 is an OPEN dk.dtu.atse.dsl.questioner.question, 37.5 refers to numeric value.
  6.1 if 5.1 >= 37.5  // Ask how many liters of water you drank, if you have fever.

  // Ask how much you exercise, if heart rate is between 65 and 130.
  4.1 if 2.4 >= 65
  4.1 if 2.4 <= 130