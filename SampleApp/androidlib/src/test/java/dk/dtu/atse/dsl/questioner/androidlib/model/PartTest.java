package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PartTest {

  Part part;
  static final String id = "Test Id";
  static final Frequency frequency = Frequency.ONCE;

  @BeforeMethod
  public void setUp() throws Exception {
    part = new Part(id, frequency);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testPart_rejectFrequency() throws Exception {
    new Part(id, null);
  }

  @Test
  public void testGetId() throws Exception {
    Assert.assertEquals(part.getId(), id);
  }

  @Test
  public void testGetId_noExplicitId() throws Exception {
    final Part idLess = new Part(null, frequency);
    Assert.assertEquals(idLess.getId(), "");
  }

  @Test
  public void testAddDependency() throws Exception {
    final Open a = new Open("Test Question A", "Test Id A", AnswerType.INT);
    part.addQuestion(a); // Dependency must be on a question in this part.
    final Open b = new Open("Test Question B", "Test Id B", AnswerType.INT);
    final Dependency dependency = new Dependency(a, b, Conditional.EQUAL, "0");
    part.addDependency(dependency);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddDependency_rejectIfSourceIsNotKnownByPart() throws Exception {
    final Open source = new Open("Source", "Source Id", AnswerType.INT);
    final Open destination = new Open("Destination", "Source Id", AnswerType.INT);
    final Dependency dependency = new Dependency(source, destination, Conditional.EQUAL, "1234");
    part.addDependency(dependency);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testAddDependency_rejectNull() throws Exception {
    part.addDependency(null);
  }

  @Test(expectedExceptions = IllegalStateException.class)
  public void testAddDependency_rejectDependenciesIfPartHasNoExplicitId() throws Exception {
    final Part idLess = new Part(null, frequency);
    final Question a = new Open("Test Question A", "Test Id A", AnswerType.INT);
    final Question b = new Open("Test Question B", "Test Id B", AnswerType.INT);
    idLess.addQuestion(a);
    final Dependency dependency = new Dependency(a, b, Conditional.EQUAL, "0");
    idLess.addDependency(dependency);
  }

  @Test
  public void testAddQuestion() throws Exception {
    final Open question = new Open("Test Question", "Test Question Id", AnswerType.INT);
    part.addQuestion(question);
  }

  @Test(expectedExceptions = NullPointerException.class)
  public void testAddQuestion_rejectNull() throws Exception {
    part.addQuestion(null);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddQuestion_rejectDuplicateIds() throws Exception {
    final String id = "Duplicate Id";
    final Open a = new Open("Test Question A", id, AnswerType.INT);
    final Open b = new Open("Test Question B", id, AnswerType.INT);
    part.addQuestion(a);
    part.addQuestion(b);
  }

  @Test
  public void testIsScheduledForToday() throws Exception {
    Assert.assertTrue(part.isScheduledForToday());
    part.markPartAsAnsweredToday();
    Assert.assertTrue(!part.isScheduledForToday());
  }

  @Test
  public void testNextQuestion() throws Exception {
    final Question q = new Open("Test Title", "Test Identifier", AnswerType.INT);
    Assert.assertNull(part.nextQuestion());
    part.addQuestion(q);
    Assert.assertEquals(part.nextQuestion(), q);
  }

  @Test
  public void testPrintLatestAnswers() throws Exception {
    part.printLatestAnswers();
  }

  @Test
  public void testMarkPartAsAnsweredToday() throws Exception {
    part.markPartAsAnsweredToday();
    Assert.assertTrue(!part.isScheduledForToday());
  }

}