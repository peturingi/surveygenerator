package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AnswerTypeTest {

  /**
   * @author Pétur
   */
  @Test
  public void testIsValid_string() throws Exception {
    Assert.assertTrue(AnswerType.TEXT.isValid("a1"));
    Assert.assertTrue(AnswerType.TEXT.isValid("Aa"));
    Assert.assertTrue(AnswerType.TEXT.isValid("A B C 1 2 3"));
    Assert.assertFalse(AnswerType.TEXT.isValid(" String "));
    Assert.assertTrue(AnswerType.TEXT.isValid("1a "));

    Assert.assertTrue(!AnswerType.TEXT.isValid(""));
    Assert.assertTrue(!AnswerType.TEXT.isValid(null));
    Assert.assertTrue(!AnswerType.TEXT.isValid(" "));
  }

  /**
   * @author Pétur
   */
  @Test
  public void testIsValid_integer() throws Exception {
    Assert.assertTrue(AnswerType.INT.isValid("-1"));
    Assert.assertTrue(AnswerType.INT.isValid("1"));
    Assert.assertTrue(AnswerType.INT.isValid("0"));
    Assert.assertTrue(AnswerType.INT.isValid("-0"));
    Assert.assertTrue(AnswerType.INT.isValid("1234"));

    Assert.assertTrue(!AnswerType.INT.isValid("1.0"));
    Assert.assertTrue(!AnswerType.INT.isValid("1,0"));
    Assert.assertTrue(!AnswerType.INT.isValid(" "));
    Assert.assertTrue(!AnswerType.INT.isValid(""));
    Assert.assertTrue(!AnswerType.INT.isValid("string"));
    Assert.assertTrue(!AnswerType.INT.isValid(null));
  }

  /**
   * @author Pétur
   */
  @Test
  public void testIsValid_float() throws Exception {

    Assert.assertTrue(AnswerType.FLOAT.isValid("-1.0"));
    Assert.assertTrue(AnswerType.FLOAT.isValid("1"));
    Assert.assertTrue(AnswerType.FLOAT.isValid("0"));
    Assert.assertTrue(AnswerType.FLOAT.isValid("-0.1"));
    Assert.assertTrue(AnswerType.FLOAT.isValid("1234"));

    Assert.assertTrue(!AnswerType.FLOAT.isValid(null), "null is not a float");
    Assert.assertTrue(!AnswerType.FLOAT.isValid("1,0"), "1,0 is not a float");
    Assert.assertTrue(!AnswerType.FLOAT.isValid(" "), "\" \" is not a float");
    Assert.assertTrue(!AnswerType.FLOAT.isValid(""), "\"\" is not a float");
    Assert.assertTrue(!AnswerType.FLOAT.isValid("string"), "string is not a float");
  }

  /**
   * @author Pétur
   */
  @Test
  public void testToString() throws Exception {
    Assert.assertTrue(AnswerType.values().length == 3, "Test needs updating, only testing three types.");
    Assert.assertEquals(AnswerType.FLOAT.toString(), "float : -?\\d+(\\.\\d*)?");
    Assert.assertEquals(AnswerType.INT.toString(), "integer : -?\\d+");
    Assert.assertEquals(AnswerType.TEXT.toString(), "string : \\w.*");
  }
}