package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class QuestionTest {

  class QuestionImpl extends Question {
    public QuestionImpl(final String title, final String id, final AnswerType expectedAnswerType) {
      super(title, id, expectedAnswerType);
    }

    @Override
    public void addAnswer(final String answer, final boolean overwrite) {
      super.addAnswer(answer, overwrite);
    }
  }

  Question question;

  @BeforeMethod
  public void setup() {
    question = new QuestionImpl("Title Test", "Id Test", AnswerType.TEXT);
  }

  /**
   * Unanswered question should indicate it needs an answer.
   *
   * @author Pétur
   */
  @Test
  public void testQuestion_NotAnsweredShouldBeAsked() throws Exception {
    Assert.assertTrue(question.shouldBeAsked());
  }

  /**
   * Ensure hasBeenAnswered is returns correct value.
   * @author Pétur
   */
  @Test
  public void testHasBeenAnswered() throws Exception {
    Assert.assertTrue(!question.hasBeenAnswered());
    question.addAnswer("Test Answer");
    Assert.assertTrue(question.hasBeenAnswered());
  }

  /**
   * toString() must not be empty.
   * @author Pétur
   */
  @Test
  public void testToString() throws Exception {
    Assert.assertTrue(StringUtils.isNotEmpty(question.toString()));
  }

  /**
   * Ensure all answers are stored.
   * @author Pétur
   */
  @Test
  public void testGetAnswers() throws Exception {
    Assert.assertEquals(question.getAnswers(), null);
    question.addAnswer("Test Answer 1", false);
    Assert.assertEquals(question.getAnswers().size(), 1);
    question.addAnswer("Test Answer 2", false);
    Assert.assertEquals(question.getAnswers().size(), 2);
  }

  /**
   * Ensure old answer can be overwritten.
   * Ensures that answers can be deleted.
   */
  @Test
  public void testAddAnswer_overwrite() throws Exception {
    Assert.assertEquals(question.getAnswers(), null);
    question.addAnswer("Test Answer", false);
    Assert.assertEquals(question.getAnswers().size(), 1);
    question.deleteAnswers();
    Assert.assertEquals(question.getAnswers(), null);
    question.addAnswer("Test Answer 1", false);
    question.addAnswer("Test Answer 2", false);
    Assert.assertEquals(question.getAnswers().size(), 2);
    question.deleteAnswers();
    Assert.assertEquals(question.getAnswers(), null);
  }

  /**
   * Check answer can be found.
   * @author Pétur
   */
  @Test
  public void testHasAnswer() throws Exception {
    final String answer = "Test Answer";
    Assert.assertTrue(!question.hasAnswer(answer));
    question.addAnswer("Something Else");
    Assert.assertTrue(!question.hasAnswer(answer));
    question.addAnswer(answer);
    Assert.assertTrue(question.hasAnswer(answer));
  }

  /**
   * Cannot ask if null is an answer.
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testHasAnswer_rejectsNull() throws Exception {
    question.hasAnswer(null);
  }

  /**
   * Rejects answer of wrong type.
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddAnswer_rejectsAnswersOfWrongType() throws Exception {
    final Question howOldAreYou = new QuestionImpl("How old are you", "1", AnswerType.INT);
    howOldAreYou.addAnswer("Not A Number");
  }

  /**
   * Can not work with a null asker.
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testAccept_rejectsNull() throws Exception {
    question.acceptPresenter(null);
  }

}