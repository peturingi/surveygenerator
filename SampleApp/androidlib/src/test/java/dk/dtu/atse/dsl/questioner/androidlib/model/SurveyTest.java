package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SurveyTest {

  /**
   * Survey must not have <code>null</code> as title.
   * @author Pétur
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testSurvey_rejectNullTitle() throws Exception {
    new Survey(null, "testSurvey_rejectNullTitle");
  }

  /**
   * Survey must not have an empty title.
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testSurvey_rejectEmptyTitle() throws Exception {
    final String id_notRelevant = "testSurvey_rejectEmptyTitle";
    new Survey("", id_notRelevant);
  }

  /**
   * A survey must have a unique id, when an id is provided.
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testSurvey_rejectIdReuse() throws Exception {
    final String id = "testSurvey_rejectIdReuse";
    final Survey s = new Survey("testSurvey_rejectIdReuse", id);
    new Survey("testSurvey_rejectIdReuse", id);
  }

  /**
   * A surveys id is optional.
   * @author Pétur
   */
  @Test
  public void testSurvey_idIsOptional() throws Exception {
    final String title = "testSurvey_idIsOptional";
    final String noId = null;
    Survey noId1 = new Survey(title, noId);
    Survey noId2 = new Survey(title, noId);
  }

  /**
   * A survey without an id, returns empty id.
   * @author Pétur
   */
  @Test
  public void testSurvey_noIdIsEmptyString() throws Exception {
    Assert.assertTrue(StringUtils.equals(new Survey("testSurvey_noIdIsEmptyString", null).getId(), ""));
  }

  /**
   * A survey may use a previously used id if its previous user has been destroyed.
   * @author Pétur
   * TODO: This test is seemingly not executable in an Android environment, as the environment does
   *       not support the java.lang.management. It is disabled, until it can be determined if it needs
   *       to be redesigned, or can be deleted entirely.
   */
  @Test(timeOut = 5000, enabled = false)
  public void testSurvey_acceptsIdReuseIfOldInstanceHasBeenDestroyed() throws Exception {
    //final String id = "testSurvey_acceptsIdReuseIfOldInstanceHasBeenDestroyed";
    //Survey survey = new Survey("testSurvey_acceptsIdReuseIfOldInstanceHasBeenDestroyed", id);

    /**
     * Release the survey.
     * Wait for gc to run before attempting to reuse the released surveys id.
     * Timeout in test definition ensures this will not run forever.
     */
    /*final List<Long> gcStats = getGarbageCollectorStatistics();
    survey = null;
    System.gc();
    while (gcStats.equals(getGarbageCollectorStatistics()))
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
      // Ignore. Go back to sleep.
      }

    // Throws an exception and fails the test if id is currently in use.
    new Survey("testSurvey_acceptsIdReuseIfOldInstanceHasBeenDestroyed", id);*/
  }

  /**
   * @author Pétur
   */
  /*private List<Long> getGarbageCollectorStatistics() {
    final List<GarbageCollectorMXBean> beans = ManagementFactory.getGarbageCollectorMXBeans();
    final List<Long> stats = new ArrayList<Long>(beans.size());
    for (GarbageCollectorMXBean gcBean : beans) {
      stats.add(gcBean.getCollectionCount());
    }
    return stats;
  }*/

  /**
   * Id returned by <code>getId()</code> must be same as used when the survey was created.
   * @author Pétur
   */
  @Test
  public void testGetId() throws Exception {
    final String expectedId = "testGetId";
    Assert.assertEquals(new Survey("testGetId", expectedId).getId(), expectedId);
  }

  /**
   * Title returned by <code>getTitle()</code> must be same as used when survey was created.
   * @author Pétur
   */
  @Test
  public void testGetTitle() throws Exception {
    final String expectedTitle = "testGetTitle";
    Assert.assertEquals(new Survey(expectedTitle, "testGetTitle").getTitle(), expectedTitle);
  }

  /**
   * Not possible to add null as part.
   * @author Pétur
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testAddPart_rejectNull() throws Exception {
    final Survey survey = new Survey("testAddPart_rejectNull", "testAddPart_rejectNull");
    survey.addPart(null);
  }

  /**
   * Adding parts does not cause exceptions.
   */
  @Test
  public void testAddPart() throws Exception {
    final Survey survey = new Survey("testAddPart", "testAddPart");
    final Part a = new Part("Test Part A", Frequency.ONCE);
    final Part b = new Part("Test Part B", Frequency.ONCE);
    survey.addPart(a);
    survey.addPart(b);
  }

  /**
   * Not possible to add two parts with same id.
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddPart_partsMustHaveUniqueIds() throws Exception {
    final Survey survey = new Survey("testAddPart_partsMustHaveUniqueIds", "testAddPart_partsMustHaveUniqueIds");
    final String id = "Test Part";
    final Part a = new Part(id, Frequency.ONCE);
    final Part b = new Part(id, Frequency.ONCE);
    survey.addPart(a);
    survey.addPart(b);
  }

}