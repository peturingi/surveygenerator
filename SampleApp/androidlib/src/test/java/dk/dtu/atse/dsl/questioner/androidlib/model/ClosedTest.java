package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ClosedTest {

  private final String title = "Test Title";
  private final String id = "Test Id";
  private final AnswerType type = AnswerType.TEXT;
  private final int min = 1;
  private final int max = 2;
  private Option optionOne = new Option("Test Option 1", "Test Id 1");
  private Option optionTwo = new Option("Test Option 2", "Test Id 2");

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_maxMustBeLargerThanMin() throws Exception {
    new Closed(title, id, type, max, min, optionOne, optionTwo);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testClosed_titleCanNotBeNull() throws Exception {
    new Closed(null, id, type, min, max, optionOne, optionTwo);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_titleCanNotBeBlank() throws Exception {
    new Closed("", id, type, min, max, optionOne, optionTwo);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_minMustBePositive() throws Exception {
    // It follows that max can not be negative as min<=max
    new Closed(title, id, type, -1, max, optionOne, optionTwo);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_idCanNotBeEmpty() throws Exception {
    new Closed(title, "", type, min, max, optionOne, optionTwo);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_idCanNotBeBlank() throws Exception {
    new Closed(title, " ", type, min, max, optionOne, optionTwo);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_atLeastAsManyOptionsAsMin() throws Exception {
    new Closed(title, " ", type, max, max, optionOne);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testClosed_maxCanNotBeZero() throws Exception {
    new Closed(title, " ", type, 0, 0, null);
  }

  /**
   * @author Pétur
   */
  @Test
  public void testClosed() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionTwo);
    Assert.assertEquals(title, closed.getTitle());
    Assert.assertEquals(id, closed.getId());
    Assert.assertEquals(type, closed.getExpectedAnswerType());
    Assert.assertEquals(min, closed.getMinAnswers());
    Assert.assertEquals(max, closed.getMaxAnswers());
    Assert.assertTrue(closed.getOptions().contains(optionOne));
    Assert.assertTrue(closed.getOptions().contains(optionTwo));
  }

  /**
   * @author Pétur
   */
  @Test
  public void testToString() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionTwo);
    closed.addAnswer(optionOne.getTitle());
    Assert.assertTrue(StringUtils.isNotBlank(closed.toString()));
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = OutOfMemoryError.class)
  public void testAddAnswer_NotMoreAnswersThanMax() throws Exception {
    final Closed closed = new Closed(title, id, type, min, 1, optionOne, optionTwo);
    closed.addAnswer(optionOne.getTitle());
    closed.addAnswer(optionTwo.getTitle());
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddAnswer_AnswerCanNotBeBlank() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionTwo);
    closed.addAnswer("");
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testAddAnswer_AnswerCanNotBeNull() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionTwo);
    closed.addAnswer(null);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testAccept_rejectsNull() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionTwo);
    closed.acceptPresenter(null);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddAnswer_rejectAnswerWhichIsNotDefinedByAnOption() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionTwo);
    closed.addAnswer("This answer is not defined as the id of an option.");
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddAnswer_optionsMustHaveUniqueId() throws Exception {
    final Closed closed = new Closed(title, id, type, min, max, optionOne, optionOne);
  }

}