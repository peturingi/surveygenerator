package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ConditionalTest {

  @Test
  public void testCompare_less() throws Exception {
    Assert.assertTrue(Conditional.LESS.compare("1", "2", AnswerType.INT));
    Assert.assertFalse(Conditional.LESS.compare("2", "1", AnswerType.INT));
  }

  @Test
  public void testCompare_lessEqual() throws Exception {
    Assert.assertTrue(Conditional.LESS_EQUAL.compare("1", "1", AnswerType.INT));
    Assert.assertTrue(Conditional.LESS_EQUAL.compare("1", "2", AnswerType.INT));
    Assert.assertFalse(Conditional.LESS_EQUAL.compare("2", "1", AnswerType.INT));
  }

  @Test
  public void testCompare_equal() throws Exception {
    Assert.assertTrue(Conditional.EQUAL.compare("1", "1", AnswerType.INT));
    Assert.assertFalse(Conditional.EQUAL.compare("1", "2", AnswerType.INT));
  }

  @Test
  public void testCompare_moreEqual() throws Exception {
    Assert.assertTrue(Conditional.MORE_EQUAL.compare("2", "2", AnswerType.INT));
    Assert.assertTrue(Conditional.MORE_EQUAL.compare("3", "2", AnswerType.INT));
    Assert.assertFalse(Conditional.MORE_EQUAL.compare("1", "2", AnswerType.INT));
  }

  @Test
  public void testCompare_more() throws Exception {
    Assert.assertTrue(Conditional.MORE.compare("2", "1", AnswerType.INT));
    Assert.assertFalse(Conditional.MORE.compare("1", "2", AnswerType.INT));
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testCompare_rejectsInvalidNumber() throws Exception {
    Assert.assertTrue(Conditional.MORE.compare("a", "a", AnswerType.INT));
  }
}