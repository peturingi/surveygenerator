package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OptionTest {

  /**
   * @author Pétur
   */
  @Test
  public void testIdIsOptional() throws Exception {
    new Option("Test Option", null);
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testTitleMustNotBeBlank() throws Exception {
    new Option("  ", "Test Id");
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testIdMustNotBeBlank() throws Exception {
    new Option("Test Title", " ");
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testTitleMustNotBeEmpty() throws Exception {
    new Option("Test Title", "");
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = NullPointerException.class)
  public void testTitleIsRequired() throws Exception {
    new Option(null, "Test Id");
  }

  /**
   * @author Pétur
   */
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testTitleCanNotBeEmpty() throws Exception {
    new Option("", "Test Id");
  }

  /**
   * @author Pétur
   */
  @Test
  public void testGetTitle() throws Exception {
    final String title = "Test Title";
    Assert.assertTrue(StringUtils.equals(new Option(title, "Test Tag").getTitle(), title));
  }

  /**
   * @author Pétur
   */
  @Test
  public void testGetId() throws Exception {
    final String id = "Test Id";
    Assert.assertTrue(StringUtils.equals(new Option("Test Title", id).getId(), id));
  }
}