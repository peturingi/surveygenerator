package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DependencyTest {

  static final Question source = new Open("Test Question 1", "Test Id 1", AnswerType.INT);
  static final Question destination = new Open("Test Question 2", "Test Id 2", AnswerType.INT);

  /**
   * @author Pétur
   */
  @Test
  public void testGetSource() throws Exception {
    final Dependency dependency = new Dependency(source, destination, Conditional.EQUAL, "2");
    Assert.assertEquals(dependency.getSource(), source);
  }

  /**
   * @author Pétur
   */
  @Test
  public void testGetDestination() throws Exception {
    final Dependency dependency = new Dependency(source, destination, Conditional.EQUAL, "2");
    Assert.assertEquals(dependency.getDestination(), destination);
  }

  /**
   * @author Pétur
   */
  @Test
  public void testGetValue() throws Exception {
    final String value = "2";
    final Dependency dependency = new Dependency(source, destination, Conditional.EQUAL, value);
    Assert.assertEquals(dependency.getValue(), value);
  }

  /**
   * @author Pétur
   */
  @Test
  public void testIsSatisfied() throws Exception {
    final String value = "2";
    source.addAnswer(value);
    final Dependency dependency = new Dependency(source, destination, Conditional.EQUAL, value);
    Assert.assertFalse(dependency.isSatisfied());
    destination.addAnswer(value);
    Assert.assertTrue(dependency.isSatisfied());

    Question a = new Open("testIsSatisfied", "testIsSatisfied_a", AnswerType.INT);
    Question b = new Open("testIsSatisfied", "testIsSatisfied_b", AnswerType.INT);
    b.addAnswer("1");
    final Dependency bLessThanTwo = new Dependency(a, b, Conditional.LESS, "2");
    Assert.assertTrue(bLessThanTwo.isSatisfied());
  }

  /**
    +--------+ 
    |   src  | 
    +--------+ 
           |   
      ^    |   
      |    |   
      |    |   
      |    |   
      |    v   
      |        
    +--------+ 
    |   dst  | 
    +--------+ 
   * @throws Exception
   */
  @Test(expectedExceptions = CyclicDependencyException.class)
  public void testTwoDependenciesCanNotBeCyclic() throws Exception {
    final Part p = new Part("PartId", Frequency.ONCE);
    p.addQuestion(source);
    p.addQuestion(destination);

    /* Build and add dependency to a part in the survey, expecting an exception. */
    final Dependency up = new Dependency(destination, source, Conditional.EQUAL, "0");
    final Dependency down = new Dependency(source, destination, Conditional.EQUAL, "0");
    p.addDependency(up);
    p.addDependency(down);
    new Survey.Builder().id("Some Test Id").title("Some Title").part(p).build();
  }


  /**
     +-------+      +--------+
     | src   |----->| dst    |
     +-------+      +--------+
          ^              |    
          |              |    
          |              |    
          |              |    
          |              |    
          |              |    
        +-------+        |    
        | third | <------+    
        +-------+             

   * @throws Exception
   */
  @Test(expectedExceptions = CyclicDependencyException.class)
  public void testThreeDependenciesCanNotBeCyclic() throws Exception {
    final Part p = new Part("PartId", Frequency.ONCE);
    p.addQuestion(source);
    p.addQuestion(destination);

    final Question third = new Open("third", "third_third", AnswerType.INT);
    p.addQuestion(third);

    /* Build and add dependency to a part in the survey, expecting an exception. */
    final Dependency toSecond = new Dependency(source, destination, Conditional.EQUAL, "0");
    final Dependency toThird = new Dependency(destination, third, Conditional.EQUAL, "0");
    final Dependency toFirst = new Dependency(third, source, Conditional.EQUAL, "0");
    p.addDependency(toSecond);
    p.addDependency(toThird);
    p.addDependency(toFirst);
    new Survey.Builder().id("Three").title("Title Three").part(p).build();
  }

  @Test
  public void testTwoDependenciesAreNotCyclic() throws Exception {
    /* Build the survey */
    final Part p = new Part("PartId", Frequency.ONCE);
    p.addQuestion(source);
    p.addQuestion(destination);

    /* Build and add dependency to a part in the survey, expecting an exception. */
    final Dependency up = new Dependency(destination, source, Conditional.EQUAL, "0");
    p.addDependency(up);

    new Survey.Builder().id("Test Id").title("Random Title").part(p).build();
  }

}