package dk.dtu.atse.dsl.questioner.androidlib.model;

import java.util.Set;

/**
 * Defines the interface for a controller which allows a client to ask for questions to ask.
 * The client can then provide answers to the questions.
 */
public interface Operational {

  /**
   * Provide answers a question.
   *
   * @param question a question which has been answered.
   * @param answers the answer(s) to the question.
   * @author Pétur
   */
  void answer(final Question question, final Set<String> answers);
}
