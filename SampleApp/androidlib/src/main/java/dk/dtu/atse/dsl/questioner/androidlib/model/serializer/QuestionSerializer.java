package dk.dtu.atse.dsl.questioner.androidlib.model.serializer;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import dk.dtu.atse.dsl.questioner.androidlib.model.Answer;
import dk.dtu.atse.dsl.questioner.androidlib.model.Question;

/**
 * Customer GSON serializer for the question types.
 * Specifically only includes answers, that are newer than a given date.
 * @author Jesper
 */
public class QuestionSerializer implements JsonSerializer<Question> {
    private final Date timeBoundery;

    /**
     * Creates a new instance of this serializer
     * @param timeBoundery Boundry for answer ages. Answers created before this
     *                     instant will not be serialized.
     */
    public QuestionSerializer(Date timeBoundery) {
        this.timeBoundery = timeBoundery;
    }

    @Override
    public JsonElement serialize(Question src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject thisJson = new JsonObject();
        thisJson.addProperty("id", src.getId());

        List<Answer> answers = src.getAnswersAddedSince(timeBoundery);
        JsonElement answersJson;

        thisJson.add("answers", context.serialize(answers));

        return thisJson;
    }
}
