package dk.dtu.atse.dsl.questioner.androidlib.model;

import com.google.gson.annotations.Expose;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DateUtils;

import java.util.*;

/**
 * A part is a group of questions.
 * A part is scheduled.
 * The schedule is such that the part is presented to the user as often as indicated by a frequency.
 */
public class Part {

  /**
   * Convenience class for easily constructing survey parts for debugging.
   */
  public static class Builder {
    private String id = null;
    private Frequency frequency = null;
    private List<Question> questions = new ArrayList<Question>();
    private List<Dependency> dependencies = new ArrayList<Dependency>();
    public Builder id(final String id) {
      this.id = id;
      return this;
    }
    public Builder frequency(final Frequency frequency) {
      this.frequency = frequency;
      return this;
    }
    public Builder question(final Question question) {
      questions.add(question);
      return this;
    }
    public Builder dependency(final Dependency dependency) {
      dependencies.add(dependency);
      return this;
    }
    public Part build() {
      final Part part = new Part(id, frequency);
      for (Question q: questions)
        part.addQuestion(q);
      for (Dependency d: dependencies)
        part.addDependency(d);
      return part;
    }
  }

  /**
   * Frequency indicates how often the questions in this part are to be presented to the user.
   */
  private Frequency frequency = null;

  /**
   * This parts identifier.
   * It is optional and defaults to "".
   */
  @Expose
  private String id = null;

  /**
   * Indicates when the questions in this part were last answered.
   */
  private Date lastAnswered = null;

  /**
   * The questions belonging to this part.
   */
  @Expose
  private List<Question> questions = new ArrayList<Question>();
  List<Question> getQuestions() { return questions; }

  Set<Dependency> getDependencies() {
    return dependencies;
  }

  /**
   * Dependencies between a question in this part, and another question (possibly in another part).
   */
  private Set<Dependency> dependencies = new HashSet<Dependency>();

  /**
   * Get this parts identifier.
   *
   * @return this parts identifier.
   * @author Pétur
   */
  public String getId() {
    return id;
  }

  /**
   * Class Constructor.
   *
   * @param id (optional) defaults to "" if null.
   * @param frequency When should the questions in this part be asked? The frequency is based on the current date.
   * @throws NullPointerException frequency was null.
   * @throws IllegalArgumentException id was blank.
   * @author Pétur
   */
  Part(final String id, final Frequency frequency) {
    Validate.notNull(frequency);
    if (id != null)
      Validate.notBlank(id);

    this.id = (id == null) ? "" : id;
    this.frequency = frequency;
  }

  /**
   * Add a dependency to this part.
   *
   * @param dependency dependency containing a source question known by this part.
   * @throws IllegalArgumentException dependency was null.
   * @throws IllegalArgumentException dependencies source is not a question known by this part.
   * @throws IllegalStateException trying to set a dependency on a question in a part which does not have an id.
   * It is not possible to refer to parts without their id, adding a dependency on an anonymous part does not make sense.
   * @author Pétur
   */
  protected void addDependency(final Dependency dependency) {
    Validate.notNull(dependency);
    Validate.isTrue(questions.contains(dependency.getSource()), "dependency is not on a question in this part.");
    if (StringUtils.isEmpty(id))
      throw new IllegalStateException("A part without an explicit id cannot hold dependencies as it can not be referred to.");

    dependencies.add(dependency);
  }

  /**
   * Add a question to this part.
   * The question must have a unique id (when an id is specified) among other questions known by this part.
   *
   * @param question the question to add to this part.
   * @throws IllegalArgumentException if question does not have a unique id.
   * @throws NullPointerException question was null.
   * @author Jesper
   */
  protected void addQuestion(final Question question) {
    Validate.notNull(question);

		for (Question q : questions) {
			Validate.isTrue(!q.getId().equals(question.getId()));
		}

    questions.add(question);
  }

  /**
   * Gives an indication of whether the part has been scheduled for today.
   *
   * @return true if the part is to be asked today; false otherwise.
   * @author Pétur
   * @author Jesper
   */
  boolean isScheduledForToday() {
    boolean scheduled = false;
    if (lastAnswered == null)
      // Unanswered questions should be answered today.
      scheduled = true;
    else if (frequency == Frequency.DAILY && lastAnswered.before(new Date()))
      scheduled = true;
    else if (frequency == Frequency.WEEKLY && lastAnswered.before(DateUtils.addWeeks(new Date(), -1)))
      scheduled = true;
    else if (frequency == Frequency.MONTHLY && lastAnswered.before(DateUtils.addMonths(new Date(), -1)))
      scheduled = true;
    else
      scheduled = false;

    return scheduled;
  }

  /**
   * Get the question to be asked.
   *
   * @return next question to be asked; null if none can be asked at this time.
   * @author Pétur
   */
  Question nextQuestion() {
    for (Question q: questions)
      if (q.shouldBeAsked() && dependenciesSatisfied(q))
        return q;
    return null;
  }

  /**
   * Print latest answers to the console.
   *
   * @author Jesper
   */
  void printLatestAnswers() {
		for (Question question : questions) {
			if (question.hasBeenAnswered()) {
				System.out.println(question.toString());
			}
		}
  }

  /**
   * Mark this part as answered today.
   *
   * @author Pétur
   */
  void markPartAsAnsweredToday() {
    lastAnswered = new Date();
  }

  /**
   * Evaluates whether all dependencies on a question are satisfied.
   *
   * @return true if satisfied; false otherwise.
   * @throws IllegalArgumentException question was null.
   * @author Pétur
   */
  private boolean dependenciesSatisfied(final Question question) {
    Validate.notNull(question);

    final Set<Dependency> dependenciesOnQuestion = dependenciesForQuestion(question);
    for (Dependency d: dependenciesOnQuestion) {
      if (!d.isSatisfied())
        return false;
    }
    return true;
  }

  /**
   * Get a list of questions on which a given question depends.
   *
   * @param question the question on questions should depend.
   * @return list of question which depend on questions answer.
   * @throws IllegalArgumentException question was null.
   * @author Pétur
   */
  private Set<Dependency> dependenciesForQuestion(final Question question) {
    Validate.notNull(question);

    Set<Dependency> result = new HashSet<Dependency>();
    for (Dependency d: this.dependencies)
      if (d.getSource().equals(question))
        result.add(d);

    return result;
  }

  // TODO REMOVE EVERYTHING BELOW THIS LINE IN PRODUCTION.

  /**
   * DEVELOPMENT METHOD, REMOVE IN PRODUCTION CODE.
   * @param days Number of days to move back by.
   * @author Pétur
   * @author Jesper
   */
  public void _moveAnsweredDateBack(int days) {
    if (this.lastAnswered == null)
      this.lastAnswered = new Date();
    Date old = new Date(lastAnswered.getTime());
		this.lastAnswered = DateUtils.addDays(lastAnswered, -days);

    if (frequency == Frequency.DAILY)
      clearOldAnswers();
    if (frequency == Frequency.WEEKLY &&
            (DateUtils.addWeeks(lastAnswered, 1).equals(old) || DateUtils.addWeeks(lastAnswered, 1).before(old)))
      clearOldAnswers();
    if (frequency == Frequency.MONTHLY &&
            (DateUtils.addMonths(lastAnswered, 1).equals(old) || DateUtils.addMonths(lastAnswered, 1).before(old)))
      clearOldAnswers();
  }

  /**
   * Forget all answers previously provided to questions in this part.
   *
   * @author Pétur
   */
  private void clearOldAnswers() {
    for (Question q: questions)
      q.deleteAnswers();
  }

}