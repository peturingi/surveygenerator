package dk.dtu.atse.dsl.questioner.androidlib.model;

/**
 * Frequency is used to indicate how often a question should be asked.
 */
enum Frequency {
  /**
   * The ONCE.
   */
  ONCE,
  /**
   * The DAILY.
   */
  DAILY,
  /**
   * The WEEKLY.
   */
  WEEKLY,
  /**
   * The MONTHLY.
   */
  MONTHLY;
}