package dk.dtu.atse.dsl.questioner.androidlib;

/**
 * @author Jesper
 */
public interface QuestionAvailabilityChangeListener {

  public void questionAvailabilityChanged (boolean questionAvailable);
}
