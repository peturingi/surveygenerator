package dk.dtu.atse.dsl.questioner.androidlib;

import dk.dtu.atse.dsl.questioner.androidlib.model.SurveyProvider;

import java.util.HashMap;

/**
 * Manages survey provider, by creating them on demand
 * and holding references to them, allowing static access
 * to all survey providers across the application.
 * @author Jesper
 */
public class SurveyProviderManager {

  private static HashMap<String,SurveyProvider> surveyProviderMap;

  static {
    surveyProviderMap = new HashMap<String,SurveyProvider>();
  }

  /**
   * Builds a new survey provider based on the provided configuration.
   * After creation, the survey provider is added to the manager's map.
   * @param configuration The survey configuration to use.
   * @return The created survey provider.
   * @throws KeyInUseException If the title of the provided survey already
   * is known to this manager (has been used by a previously created survey),
   * the survey cannot be created.
   */
  public static SurveyProvider buildSurveyProvider (final String configuration) throws KeyInUseException {
    SurveyProvider newSurveyProvider = new SurveyProvider(configuration);

    if (surveyProviderMap.containsKey(newSurveyProvider.getSurveyTitle())) {
      throw new KeyInUseException("Another survey provider already exists with the key: '"
          + newSurveyProvider.getSurveyTitle() + "'.");
    }

    surveyProviderMap.put(newSurveyProvider.getSurveyTitle(), newSurveyProvider);

    return newSurveyProvider;
  }

  /**
   * Get the survey provider using this key.
   * @param key The key uniquely referencing this survey provider.
   * @return The survey provider referred to by the provided key.
   * @throws NoSuchKeyException The provided key cannot be found.
   */
  public static SurveyProvider getSurveyProvider (final String key) throws NoSuchKeyException {
    SurveyProvider surveyProvider = surveyProviderMap.get(key);

    if (surveyProvider == null) {
      throw new NoSuchKeyException("Found no surveyprovider with key: '" + key + "'.");
    }

    return surveyProvider;
  }

  /**
   * Exception indicating that this key is already in use by the
   * manager.
   */
  public static class KeyInUseException extends Exception {
    KeyInUseException(String detailMessage) {
      super(detailMessage);
    }
  }

  /**
   * Exception indicating that this key cannot be found in the
   * manager's survey provider map.
   */
  public static class NoSuchKeyException extends Exception {
    NoSuchKeyException(String detailMessage) {
      super(detailMessage);
    }
  }
}
