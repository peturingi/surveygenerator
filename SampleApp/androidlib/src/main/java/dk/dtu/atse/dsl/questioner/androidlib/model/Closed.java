package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.*;

/**
 * Closed represents a closed question.
 * A closed question contains one or more options.
 * It takes one or more answers, all of which are an id matchine one of its options.
 */
public class Closed extends Question {

  /**
   * The minimum number of answers which must be provided for this question.
   */
  protected int minAnswers;

  /**
   * The maximum number of answers which can be provided for this question.
   */
  protected int maxAnswers;

  /**
   * The options (available answers) for this question.
   */
  protected List<Option> options = new ArrayList<Option>();

  /**
   * Get the minimum number of answers which must be provided for this question.
   *
   * @return the minimum number of answers to be provided.
   * @author Pétur
   */
  public int getMinAnswers() {
    return minAnswers;
  }

  /**
   * Get the maximum number of answers which may be provided for this question.
   * @return the maximum number of answers which may be provided.
   * @author Pétur
   */
  public int getMaxAnswers() {
    return maxAnswers;
  }

  /**
   * Get all options (valid answers to this question).
   *
   * @return all options for this question.
   * @author Pétur
   */
  public List<Option> getOptions() {
    return options;
  }

  /**
   * Class Constructor.
   *
   * @param title this questions title.
   * @param id a uuid for this question.
   * @param expectedAnswerType the expected answer type of answers to this question.
   * @param minAnswers the minimum number of answers which must be provided for this question.
   * @param maxAnswers the maximum number of answers which may be provided for this question.
   * @param options the options for this question.
   * @throws IllegalArgumentException min was negative.
   * @throws IllegalArgumentException max was zero.
   * @throws IllegalArgumentException max was smaller than min.
   * @throws IllegalArgumentException title was blank.
   * @throws IllegalArgumentException id was empty or blank.
   * @throws IllegalArgumentException number of options was smaller than min.
   * @throws IllegalArgumentException two or more options have the same id.
   * @throws NullPointerException title was null.
   * @author Pétur
   */
    Closed(final String title,
                final String id,
                final AnswerType expectedAnswerType,
                final int minAnswers,
                final int maxAnswers,
                final Option... options) {
    super(title, id, expectedAnswerType);
    Validate.isTrue(minAnswers <= maxAnswers, "Min must not be bigger than max.");
    Validate.notNull(options, "Must specify options.");
    Validate.inclusiveBetween(0, Integer.MAX_VALUE, minAnswers, "Min must be positive.");
    Validate.inclusiveBetween(minAnswers, options.length, maxAnswers, "violation of 'minAnswer <= maxAnswers <=|options|'");
    Validate.isTrue(haveUniqueIds(options), "Two or more options have the same id.");

    for (Option o: options)
      this.options.add(o);

    this.minAnswers = minAnswers;
    this.maxAnswers = maxAnswers;
  }

  /**
   * Evaluate whether the all options have a unique id.
   *
   * @param options options to evaluate.
   * @return true if no two options have the same id; false otherwise.
   * @author Pétur
   */
  private boolean haveUniqueIds(final Option... options) {
    final Set<String> ids = new HashSet<String>();
    for (Option o: options)
      ids.add(o.getId());
    return ids.size() == options.length;
  }

  /**
   * Get a string representation of this question (and its answers, if any).
   *
   * @return A string representation of this question.
   * @author Pétur
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(id + " : " + title + " : ");
    for (Answer a: answers)
      sb.append(a + " ");
    return sb.toString();
  }

  /**
   * Adds an answer to the list of answers.
   * @param answer an answer to this question.
   * @throws OutOfMemoryError the upper limit of allowed answers has been broken.
   */
  @Override
  public void addAnswer(final String answer) {
    Validate.notNull(answer);
    if (answers.size()+1 > maxAnswers)
      throw new OutOfMemoryError("Can not store more than " + maxAnswers + " answers.");
    Validate.isTrue(isAnswerAnOption(answer), "'" + answer + "' does not match an option.");

    super.addAnswer(answer, false);
  }

  /**
   * Is an answer a valid option?
   * An answer is a valid option when it matches the title of one of this questions options.
   * @param answer the answer to check.
   * @return true if the answer is a valid option; false otherwise.
   * @throws NullPointerException answer was null.
   * @author Pétur
   */
  private boolean isAnswerAnOption(final String answer) {
    Validate.notNull(answer);
    for (Option o: options)
      if (StringUtils.equals(o.getTitle(), answer))
        return true;
    return false;
  }

  /**
   * Accept an interrogator visitor on this question. The interrogator
   * is expected to present the question to a user after having called
   * this method.
   * @param interrogator The requester the question.
   * @author Pétur (The basic visitor pattern)
   * @author Jesper (Extension of concept to work with a GUI)
   */
  @Override
  public void acceptPresenter(Interrogator interrogator) {
    Validate.notNull(interrogator);
    interrogator.ask(this);
  }

  /**
   * Accept an interrogator visitor on this question. The interrogator
   * should return a set of answers to the question in response to this call.
   * @param interrogator The responder of the question.
   * @return A set of answers to this question.
   * @author Pétur (The basic visitor pattern)
   * @author Jesper (Extension of concept to work with a GUI)
   */
  @Override
  public Set<String> acceptResponder(Interrogator interrogator) {
    Validate.notNull(interrogator);
    return interrogator.answer(this);
  }
}
