package dk.dtu.atse.dsl.questioner.androidlib.model;

import com.google.gson.annotations.Expose;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;

/**
 * A survey consists of a set of parts.
 *
 * A survey has has a non-empty title and an optional identifier.
 * The identifier, if used, is a UUID which can be used to identify a specific survey.
 *
 * @see Part
 * @author Pétur
 */
public class Survey {

  public static class Builder {
    private String title = null;
    private String id = null;
    private List<Part> parts = new ArrayList<Part>();
    public Builder part(Part part) {
      parts.add(part);
      return this;
    }
    public Builder title(String title) {
      this.title = title;
      return this;
    }
    public Builder id(String id) {
      this.id = id;
      return this;
    }

    /**
     * @throws CyclicDependencyException A cycle was found among dependencies.
     */
    public Survey build() {
      final Survey survey = new Survey(title, id);
      for (Part p: parts)
        survey.addPart(p);
      if (survey.hasCyclicDependencies())
        throw new CyclicDependencyException("Detected a cyclic dependency.");
      return survey;
    }
  }

  /**
   * This surveys title.
   */
  private String title;

  /**
   * This surveys UUID.
   */
  @Expose
  private String id;

  /**
   * This surveys parts.
   */
  @Expose
  private List<Part> parts = new ArrayList<Part>();

  /**
   * A weak set.
   * Knows of every currently existing survey.
   * Used when verifying that the provided identifier of a survey about to be created, is a uuid.
   *
   * @author Pétur
   */
  static final private Set<Survey> existingSurveys = Collections.newSetFromMap(new WeakHashMap<Survey,Boolean>());

  /**
   * Get this surveys id.
   *
   * @return this surveys uuid which can be used to identify this survey; <code>""</code> (empty string) in case an id was not set.
   * @author Pétur
   */
  public String getId() {
    return id;
  }

  /**
   * Get this surveys title.
   *
   * @return this surveys title.
   * @author Pétur
   */
  public String getTitle() {
    return title;
  }

  /**
   * Class constructor.
   *
   * @param title a title for the survey. The title may not be null or empty.
   * @param id a uuid for this survey. The id is optional; if provided it must be a uuid.
   * @throws NullPointerException <code>title == null</code>.
   * @throws IllegalArgumentException title was empty.
   * @throws IllegalArgumentException the provided id is currently in use by another survey.
   * @author Pétur
   */
  Survey(final String title, final String id) {
    Validate.notNull(title);
    Validate.notEmpty(title);
    Validate.isTrue(id == null || isSurveyUUID(id), "A survey has already been created with id: " + id);

    this.title = title;
    this.id = (id == null) ? "" : id;
    existingSurveys.add(this);
  }

  /**
   * Check whether an id is in use by a part known to this survey.
   *
   * @param id the id to check.
   * @return true if id is a non-empty string which can uniquely identify this survey from all living surveys; false otherwise.
   * @author Pétur
   */
  private boolean isSurveyUUID(final String id) {
    if (StringUtils.isEmpty(id))
      return false;

    for (Survey s: existingSurveys)
      if (!s.getId().equals(""))
        if (s.getId().equals(id))
          return false;

    return true;
  }

  /**
   * Add part to this survey.
   * The part must be uniquely identifiable, from other parts known by this survey, by its id.
   *
   * @param part the part to add to this survey.
   * @throws IllegalArgumentException part has already been added to this survey.
   * @throws IllegalArgumentException this survey already has a part with same identifier as the part supplied.
   * @throws NullPointerException part is null.
   * @author Pétur
   */
  void addPart(final Part part) {
    Validate.notNull(part);
    Validate.isTrue(!parts.contains(part));
    for (Part p: parts)
      Validate.isTrue(!p.getId().equals(part.getId()));

    parts.add(part);
  }

  /**
   * Get next question to be asked.
   *
   * @return a question to be asked; null if none.
   * @author Pétur
   */
  public Question nextQuestion() {
    for (Part p: parts) {
      if (p.isScheduledForToday()) {
        final Question q = p.nextQuestion();
        if (q != null)
          return q;
      }
    }
    return null;
  }

  /**
   * Answers whether there is a question in this part which can be asked.
   *
   * @return true if a question needs answer; false otherwise.
   * @author Pétur
   */
  public boolean questionNeedsAnswer() {
    return nextQuestion() != null;
  }

  /**
   * Detects cyclic dependencies on questions.
   *
   * @return true if dependencies are cyclie; false otherwise
   * @author Pétur
   */
  private boolean hasCyclicDependencies() {
    final List<Dependency> dependencies = new LinkedList<Dependency>();
    final Set<Question> uniqueQuestions = new HashSet<Question>();
    for (Part p: parts)
      for (Dependency d: p.getDependencies()) {
        dependencies.add(d);
        uniqueQuestions.add(d.getSource());
        uniqueQuestions.add(d.getDestination());
      }

    final DirectedGraph<Question, DefaultEdge> directedGraph = new DefaultDirectedGraph<Question, DefaultEdge>(DefaultEdge.class);
    for (Question q : uniqueQuestions)
      directedGraph.addVertex(q);
    for (Dependency d: dependencies)
      directedGraph.addEdge(d.getSource(), d.getDestination());

    return new CycleDetector<Question, DefaultEdge>(directedGraph).detectCycles();
  }


  /**
   * _ move answered date back.
   *
   * @param days the days
   * @author Pétur
   */
// TODO Remove in production. Only for manual testing.
  public void _moveAnsweredDateBack(int days) {
    for (Part p: parts)
      p._moveAnsweredDateBack(days);
  }

}