package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.Validate;

/**
 * The enum Conditional.
 * @author Jesper
 * @author Pétur
 */
enum Conditional {
  /**
   * The LESS.
   */
  LESS,
  /**
   * The LESS_EQUAL.
   */
  LESS_EQUAL,
  /**
   * The EQUAL.
   */
  EQUAL,
  /**
   * The MORE_EQUAL.
   */
  MORE_EQUAL,
  /**
   * The MORE.
   */
  MORE;

  /**
   * Compares the left string against the right string, based on this conditional.
   * The EQUAL conditional can be used to compare both strings and numbers.
   * The other conditionals can only be used to compare strings that represent doubles.
   *
   * Example: If left = 3; right = 4; this = MORE; this returns true.
   *
   * @param left A string to be compared.
   * @param right A string to be compared.
   * @param type The data type the two strings represent.
   * @return Based on the conditional, whether the dk.dtu.atse.dsl.questioner.question.part.condition holds.
   * @throws IllegalArgumentException if any of the arguments are null.
   * @throws IllegalArgumentException if left or right are not of type type.
   */
  public boolean compare(final String left, final String right, final AnswerType type) {
    Validate.notNull(left);
    Validate.notNull(right);
    Validate.notNull(type);
    Validate.isTrue(type.isValid(left), "left is of an invalid type.");
    Validate.isTrue(type.isValid(right), "right is of an invalid type.");

    if (!this.equals(EQUAL)) {
      switch (this) {
        case LESS:
          return type.compare(left, right) < 0;
        case LESS_EQUAL:
          return type.compare(left, right) <= 0;
        case MORE_EQUAL:
          return type.compare(left, right) >= 0;
        case MORE:
          return type.compare(left, right) > 0;
        default:
          throw new InternalError("Unknown enum type.");
      }
    } else
      return type.compare(left, right) == 0;
  }

}
