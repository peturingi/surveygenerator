package dk.dtu.atse.dsl.questioner.androidlib.model;

/**
 * @author Pétur
 */
public class CyclicDependencyException extends RuntimeException {
  public CyclicDependencyException(final String message) {
    super(message);
  }
}
