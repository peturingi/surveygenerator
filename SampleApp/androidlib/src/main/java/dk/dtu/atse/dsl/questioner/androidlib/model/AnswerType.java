package dk.dtu.atse.dsl.questioner.androidlib.model;

import java.util.regex.Pattern;

/**
 * Represents the type of an answer a question expects.
 *
 * @see Question
 */
public enum AnswerType {
  TEXT("string", "\\w.*") {
    public int compare(final String a, final String b) { return a.compareTo(b); }
  },
  INT("integer", "-?\\d+") {
    public int compare(final String a, final String b) { return Integer.valueOf(a).compareTo(Integer.valueOf(b)); }
  },
  FLOAT("float", "-?\\d+(\\.\\d*)?") {
    public int compare(final String a, final String b) { return Float.valueOf(a).compareTo(Float.valueOf(b)); }
  };

  /**
   * {@linkplain java.util.Comparator#compare(Object, Object) implementation}
   * @author Pétur
   */
  public abstract int compare(final String a, final String b);

  private final String type;
  private final String regexp;

  AnswerType(final String type, final String regexp) {
    Pattern.compile(regexp); // Check which throws PatternSyntaxException if regexp is invalid.
    this.type = type;
    this.regexp = regexp;
  }

  /**
   * Check whether the given answer is valid for this answer type.
   *
   * <p>
   * Valid answers:<br>
   * <b>TEXT</b> : Any nonblank string.<br>
   *
   * <b>INT</b>: An optional negation symbol and a sequence of digits.<br>
   *
   * <b>FLOAT</b>:   An optional negation symbol, zero or more digits.
   * There must be at least one digit in a float.<br>
   * </p>
   * @param answer The answer to check for validity against this type.
   * @return true if the answer is valid; false otherwise.
   * @author Jesper
   * @author Pétur (refactoring)
   */
  public boolean isValid(final String answer) {
    return answer == null ? false : answer.matches(regexp);
  }

  /**
   * Get a user friendly single-word description of this type.
   *
   * @return a user friendly description of this type.
   * @author Pétur
   */
  @Override
  public String toString() {
    return type + " : " + regexp;
  }
}
