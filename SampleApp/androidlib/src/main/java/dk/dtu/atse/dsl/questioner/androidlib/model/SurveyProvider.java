package dk.dtu.atse.dsl.questioner.androidlib.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dk.dtu.atse.dsl.questioner.androidlib.QuestionAvailabilityChangeListener;
import dk.dtu.atse.dsl.questioner.androidlib.model.serializer.QuestionSerializer;

import org.apache.commons.lang3.Validate;

import java.util.*;

/**
 * A SurveyProvider creates a survey based on a program.
 * It notifies a client when questions, within the survey, need to be answered.
 */
public class SurveyProvider extends Thread implements Operational {

  /**
   * Sleep for 1 second between checks for new questions.
   * This value could be raised up to 1 day, as the DSLs minimum wait time is 1 day.
   */
  private final long interval = 1000;

  private final Survey survey;
  private boolean questionAvailableAtLastCheck;
  private QuestionAvailabilityChangeListener changeListener;

  /**
   * Constructs a survey provider.
   *
   * @param configuration the program used to configure the SurveyProvider.
   * @param configuration the program used to configure the SurveyProvider.
   * @author Pétur
   */
  public SurveyProvider(final String configuration) {
    this.survey = createMockSurvey(); // TODO replace this with parsing of configuration.
    this.questionAvailableAtLastCheck = false;
    this.start();
  }

  /**
   * Populate the semantic model, as we do not have a parser.
   */
  private static Survey createMockSurvey() {

    final Closed sex = new Closed("Sex",
                                  "2",
                                  AnswerType.TEXT,
                                  1,
                                  1,
                                  new Option("Male", "1"),
                                  new Option("Female", "2"));

    final Closed ateBreakfast = new Closed("Did you eat breakfast?",
                                           "1",
                                           AnswerType.TEXT,
                                           1,
                                           1,
                                           new Option("Yes", "1"),
                                           new Option("No", "2"));

    final Closed tired = new Closed("Do you feel tired?",
                                    "3",
                                    AnswerType.TEXT,
                                    1,
                                    1,
                                    new Option("Yes", "1"),
                                    new Option("No", "2"));

    final Closed coffeeQuiz  = new Closed("How do you like your coffee?",
                                          "1",
                                          AnswerType.TEXT,
                                          1,
                                          2,
                                          new Option("with Milk", "1"),
                                          new Option("with Sugar", "2"),
                                          new Option("with Syrup", "3"),
                                          new Option("with Cookies", "4"),
                                          new Option("other", "5"));

    final Open otherCoffee = new Open("What else do you like with your coffee?", "2", AnswerType.TEXT);
    final Open howOften = new Open("For how many hours did you exercise in the passing week?", "1", AnswerType.FLOAT);
    final Open tempQuiz = new Open("What is your temperature in °C ?", "1", AnswerType.FLOAT);
    final Open waterAmount = new Open("How many liters of water have you consumed in last 24 hours?", "1", AnswerType.FLOAT);
    final Open heartRate = new Open("What is your heart rate?", "4", AnswerType.INT);
    final Open breakfastDescription = new Open("What did you eat?", "2", AnswerType.TEXT);
    final Open age = new Open("How old are you?", "1", AnswerType.INT);

    final Part coffee = new Part.Builder()
            .id("Coffee")
            .frequency(Frequency.DAILY)
            .question(coffeeQuiz)
            .question(otherCoffee)
            .dependency(new Dependency(otherCoffee, coffeeQuiz, Conditional.EQUAL, "other"))
            .build();

    final Part breakfast = new Part.Builder()
            .id("Breakfast")
            .frequency(Frequency.DAILY)
            .question(ateBreakfast)
            .question(breakfastDescription)
            .question(tired)
            .question(heartRate)
            .dependency(new Dependency(breakfastDescription, ateBreakfast, Conditional.EQUAL, "Yes"))
            .build();

    final Part healthPart =  new Part.Builder()
            .id("User Information")
            .frequency(Frequency.ONCE)
            .question(age)
            .question(sex)
            .build();

    final Part exercise = new Part.Builder()
            .id("Exercise")
            .frequency(Frequency.WEEKLY)
            .question(howOften)
            .dependency(new Dependency(howOften, age, Conditional.MORE,  "65"))
            .dependency(new Dependency(howOften, sex, Conditional.EQUAL, "Male"))
            .build();

    final Part water = new Part.Builder()
            .id("Water")
            .frequency(Frequency.DAILY)
            .question(waterAmount)
            .dependency(new Dependency(waterAmount, howOften, Conditional.MORE_EQUAL, "7"))
            .dependency(new Dependency(waterAmount, tempQuiz, Conditional.MORE, "37.5"))
            .build();

    final Part temperature = new Part.Builder()
            .id("Temperature")
            .frequency(Frequency.DAILY)
            .question(tempQuiz)
            .build();

    return new Survey.Builder()
            .title("Health")
            .id("1")
            .part(healthPart)
            .part(breakfast)
            .part(coffee)
            .part(exercise)
            .part(water)
            .part(temperature)
            .build();
  }

    /**
     * Continually check for new questions, and the change listener if
     * a new question becomes available.
     *
     * @author Jesper
     */
  @Override
  public void run() {
    while(true) {
      try {
        if (survey.questionNeedsAnswer() != questionAvailableAtLastCheck) {
          questionAvailableAtLastCheck = survey.questionNeedsAnswer();
          notifyChangeListener();
        }
        else {
          Thread.sleep(interval);
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
      catch (NullPointerException e) {
        //Ignore, attempted to add null to questionqueue
      }
    }
  }

    /**
     * Gets the next available question from the provided survey.
     * @return A question ready to be answered.
     * @throws NotAvailableException If no question is available.
     */
  public Question getNextQuestion() throws NotAvailableException {
    Question q = survey.nextQuestion();

    if (q == null) {
      throw new NotAvailableException("There are no questions available.");
    }

    return q;
  }

    /**
     * Indicates whether a new question is available.
     * @return True if there is at least one question ready for
     * answering, otherwise false.
     */
  public boolean hasNewQuestion() {
    return survey.questionNeedsAnswer();
  }

  public String getSurveyTitle() {
    return survey.getTitle();
  }

  public void setQuestionAvailabilityChangeListener (QuestionAvailabilityChangeListener listener) {
    this.changeListener = listener;
  }

    /**
     * Notifies the attached change listener that question
     * availability has changed, i.e. if a question has become available
     * when there were previously none, or there are no longer questions
     * available, when there previously were.
     * Has no effect if no change lister has been provided.
     *
     * @author Jesper
     */
  private void notifyChangeListener() {
    if (changeListener != null) {
      changeListener.questionAvailabilityChanged(survey.questionNeedsAnswer());
    }
  }

  /**
   * Register answers to a question.
   *
   * @param question the question to which the answer(s) apply.
   * @param answers the answers. If closed question, use answer titles, not identifiers.
   * @throws NullPointerException question was null.
   * @throws NullPointerException answers was null.
   * @throws IllegalArgumentException answers was empty.
   * @throws IllegalArgumentException multiple answers provided for an Open question.
   * @throws IllegalArgumentException no answer provided for a Closed question.
   * @author Pétur
   */
  //TODO: Answer strings should refer to identifiers, not titles.
  public void answer(final Question question, final Set<String> answers) {
    Validate.notNull(question);
    Validate.notNull(answers);
    Validate.notEmpty(answers);
    if (question instanceof Open)
      Validate.isTrue(answers.size() == 1, "An open question can only be given a single answer.");
    if (question instanceof Closed)
      Validate.isTrue(answers.size() != 0);

    Iterator<String> answersIterator = answers.iterator();
    question.addAnswer(answersIterator.next());
    while (answersIterator.hasNext())
      question.addAnswer(answersIterator.next(), false);

    printTimestampedAnswerToConsole(question.getAnswers());
  }

    /**
     * Parses this survey into JSON. Note that this is meant for sharing
     * answers, thus for the survey, the parts and the questions, only an the
     * ID and their subordinate parts are included.
     * @param minimumAnswerAge A timestamp indicating the oldest allowed age
     *                         for returned answers. Answers with timestamps
     *                         older than this, will not be serialized.
     * @param usePrettyPrint If true, GSON's pretty printer will be applied,
     *                       thus the result will be formatted with newlines
     *                       and indentations. If false, the result string
     *                       will not be formatted in any way.
     * @return A JSON representation of this survey's answers.
     * @author Jesper
     */
  public String getSurveyAsJson(Date minimumAnswerAge, boolean usePrettyPrint) {
      GsonBuilder builder = new GsonBuilder()
              .excludeFieldsWithoutExposeAnnotation()
              .registerTypeAdapter(Closed.class, new QuestionSerializer(minimumAnswerAge))
              .registerTypeAdapter(Open.class, new QuestionSerializer(minimumAnswerAge));

      if (usePrettyPrint) {
          builder.setPrettyPrinting();
      }

      Gson gson = builder.create();
      return gson.toJson(this.survey);
  }


  /**
   * This method is for develoment purposes only.
   * // TODO replace with a method which sends the answers to a server.
   *
   * @author Pétur
   * @author Jesper
   */
  public void printTimestampedAnswerToConsole(final List<Answer> answers) {
    System.out.println(new Date().toString() + " " + answers);
  }

    /**
     * Exception indicating that a requested resource is not available.
     *
     * @author Jesper
     */
  public class NotAvailableException extends Exception {
    NotAvailableException(String detailMessage) {
      super(detailMessage);
    }
  }
}
