package dk.dtu.atse.dsl.questioner.androidlib.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Question Type.
 *
 * @see dk.dtu.atse.dsl.questioner.androidlib.model.Open
 * @see dk.dtu.atse.dsl.questioner.androidlib.model.Closed
 */
public abstract class Question {
  /**
   * This questions title.
   */
  protected String title;
  /**
   * This question identifier.
   */
  @Expose
  protected String id;

  /**
   * this question has an expected answer type.
   * Any answer to question, which is not of the given type will be rejected.
   */
  private final AnswerType expectedAnswerType;

  /**
   * Get this questions expected answer type.
   *
   * @return this questions expected answer type.
   * @author Pétur
   */
  public AnswerType getExpectedAnswerType() {
    return expectedAnswerType;
  }

  /**
   * Answers given to this question.
   */
  protected List<Answer> answers = new ArrayList<Answer>();
  /**
   * The time at which the last answer to this question was given.
   */
  protected Date lastAnswered = null;

  /**
   * Class Constructor.
   *
   * @param title The title of the question to be asked.
   * @param id (optional) defaults to "" if null.
   * @throws NullPointerException title or expectedAnswerType was null.
   * @throws IllegalArgumentException title or id was empty or blank.
   * @author Pétur
   */
  public Question(final String title, final String id, final AnswerType expectedAnswerType) {
    Validate.notBlank(title);
    if (id != null)
      Validate.notBlank(id);
    Validate.notNull(expectedAnswerType);

    this.title = title;
    this.id = (id == null) ? "" : id;
    this.expectedAnswerType = expectedAnswerType;
  }

  /**
   * Should this question be asked.
   *
   * @return the boolean.
   * @author Pétur
   */
  boolean shouldBeAsked() {
    return answers.isEmpty();
  }

  /**
   * Has been answered.
   *
   * @return the boolean.
   * @author Pétur
   */
  boolean hasBeenAnswered() {
    return !answers.isEmpty();
  }

  /**
   * Get a string representation of this question, suitable for showing to the user in a log.
   *
   * @return a string representation of this question and its answers.
   * @author Pétur
   */
  public String toString() {
    return id + " : " + title + " : " + getAnswers();
  }

  /**
   * Get this questions id.
   *
   * @return this questions id.
   * @author Pétur
   */
  public String getId() {
    return id;
  }

  /**
   * Get time the last answer was provided to this question.
   *
   * @return time representing the time at which an answer was given.
   * @author Pétur
   */
  Date getLastAnswered() { return lastAnswered; }

  /**
   * Get this questions title.
   *
   * @return this questions title.
   * @author Pétur
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get all answers which have been provided to this question.
   *
   * @return list of answers if any; null otherwise.
   * @author Pétur
   */
  public List<Answer> getAnswers() {
    return answers.isEmpty() ? null : answers;
  }

  /**
   * Get all answers to this question, that are timestamped later
   * than the given timestamp.
   *
   * @param timeBoundary An instant indicating the maximum age of
   *                     the returned answers.
   * @return A list of answers newer than the provided timestamp.
   *         The list is empty if no such answers exist.
   * @author Jesper
   */
  public List<Answer> getAnswersAddedSince(Date timeBoundary) {
    List<Answer> result = new ArrayList<Answer>();

    if (getAnswers() != null) {
      for(Answer answer : getAnswers()) {
        if (answer.getTime().after(timeBoundary)) {
          result.add(answer);
        }
      }
    }

    return result;
  }

  /**
   * Has answer.
   *
   * @param answer the answer
   * @return the boolean
   * @throws NullPointerException answer was null.
   * @author Pétur
   */
  boolean hasAnswer(final String answer) {
    Validate.notNull(answer);

    for (Answer s: answers)
      if (s.getValue().equals(answer))
        return true;
    return false;
  }

  /**
   * Provide an answer to this question.
   * A previous answer, if any, will be overwritten.
   *
   * @param answer The answer to this question. Must be of a correct type.
   * @throws IllegalArgumentException Answer was of a wrong type.
   * @author Pétur
   */
  public void addAnswer(final String answer) throws IllegalArgumentException {
    addAnswer(answer, true);
  }

  /**
   * Sets an answer.
   *
   * @param answer The answer to this dk.dtu.atse.dsl.questioner.question. Must be of correct type.
   * @param overwrite Overwrite old answers.
   * @author Pétur
   */
  protected void addAnswer(final String answer, final boolean overwrite) {
    Validate.isTrue(expectedAnswerType.isValid(answer), answer + " is not of the expected type: " + expectedAnswerType.toString());

    if (overwrite)
      answers.clear();
    answers.add(new Answer(answer));
  }

  /**
   * Clear old answers.
   *
   * @author Pétur.
   */
  void deleteAnswers() {
    answers.clear();
  }

  /**
   * Accept an interrogator visitor on this question. The interrogator
   * is expected to present the question to a user after having called
   * this method.
   * @param interrogator The requester the question.
   * @author Pétur (The basic visitor pattern)
   * @author Jesper (Extension of concept to work with a GUI)
   */
  public void acceptPresenter(Interrogator interrogator) {
    Validate.notNull(interrogator);
    interrogator.ask(this);
  }

  /**
   * Accept an interrogator visitor on this question. The interrogator
   * should return a set of answers to the question in response to this call.
   * @param interrogator The responder of the question.
   * @return A set of answers to this question.
   * @author Pétur (The basic visitor pattern)
   * @author Jesper (Extension of concept to work with a GUI)
   */
  public Set<String> acceptResponder(Interrogator interrogator) {
    Validate.notNull(interrogator);
    return interrogator.answer(this);
  }
}
