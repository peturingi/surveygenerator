package dk.dtu.atse.dsl.questioner.androidlib.model;

import java.util.Set;

/**
 * Defines methods which know how to ask different types of questions.
 * A component wishing to handle the interaction between a user and a
 * survey, i.e. presenting questions and accepting answers to them,
 * should implement this interface.
 *
 * Serves to implement the visitor pattern.
 *
 * @author Pétur (The basic visitor pattern)
 * @author Jesper (Extension of concept to work with a GUI)
 */
public interface Interrogator {

  /**
   * Should never be invoked.
   * Report an error if this is invoked.
   */
  void ask(final Question question);

  /**
   * Provide an open question, for which an answer can later be requested.
   *
   @param question open question.
   */
  void ask(final Open question);

  /**
   * Provide a closed question, for which an answer can later be requested.
   *
   @param question closed question.
   */
  void ask(final Closed question);

  /**
   * Should never be invoked.
   * Report an error if this is invoked.
   */
  Set<String> answer(final Question question);

  /**
   * Request an answer for an open question.
   * As open questions can be marked optional, the answer might be empty.
   *
   @param question closed question.
   @return A set containing a single, possibly empty string, answer to this question.
   */
  Set<String> answer(final Open question);

  /**
   * Request an answer for a closed question.
   *
   * @param question closed question.
   * @return A set containing one or more answers to this question.
   * Each answer represents the title of the option(s) selected as answers to question.
   */
  Set<String> answer(final Closed question);
}
