package dk.dtu.atse.dsl.questioner.androidlib.model;

import com.google.gson.annotations.Expose;
import org.apache.commons.lang3.Validate;

import java.util.Date;

/**
 * Represents a single answer to a question.
 * @author Jesper
 */
public class Answer {
    @Expose
    private String value;
    @Expose
    private Date time;

    /**
     * Instantiate an answer to the given value. The answer's time
     * field is set at this time.
     * @param value Value indicating the user's answer.
     */
    public Answer(String value) {
        Validate.notNull(value);

        this.value = value;
        this.time = new Date();
    }

    public String getValue() {
        return value;
    }

    public Date getTime() {
        return time;
    }

    @Override
    public String toString() {
        return '(' + getTime().toString() + ", " + getValue() + ')';
    }
}
