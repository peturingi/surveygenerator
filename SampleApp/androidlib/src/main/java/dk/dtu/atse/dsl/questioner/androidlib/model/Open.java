package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.Validate;

import java.util.Set;

/**
 * An open question.
 * A question which accepts a general answer of a given type.
 * @author Pétur (The basic visitor pattern)
 * @author Jesper (Extension of concept to work with a GUI)
 */
public class Open extends Question {

  /**
   * Class Constructor.
   *
   * @param title The title of the question to be asked.
   * @param id (optional) defaults to "" if null.
   * @throws NullPointerException title or expectedAnswerType was null.
   * @throws IllegalArgumentException title or id was empty or blank.
   */
  Open(final String title, final String id, final AnswerType expectedAnswerType) {
    super(title, id, expectedAnswerType);
  }

  /**
   * Accept an interrogator visitor on this question. The interrogator
   * is expected to present the question to a user after having called
   * this method.
   * @param interrogator The requester the question.
   */
  @Override
  public void acceptPresenter(Interrogator interrogator) {
    Validate.notNull(interrogator);
    interrogator.ask(this);
  }

  /**
   * Accept an interrogator visitor on this question. The interrogator
   * should return a set of answers to the question in response to this call.
   * @param interrogator The responder of the question.
   * @return A set of answers to this question.
   */
  @Override
  public Set<String> acceptResponder(Interrogator interrogator) {
    Validate.notNull(interrogator);
    return interrogator.answer(this);
  }
}
