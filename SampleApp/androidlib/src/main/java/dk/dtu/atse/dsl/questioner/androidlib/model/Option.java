package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.Validate;

/**
 * An option is an answer to a question.
 */
public class Option {

  /**
   * This options title.
   * The title represents an answer to a question.
   */
  private final String title;

  /**
   * This options id.
   */
  private final String id;

  /**
   * Constructs a new option using the parameters.
   * @param title The options title as shown to the user. Must not be empty or blank.
   * @param id (optional) pass null if not needed.
   * @throws NullPointerException title was null.
   * @throws IllegalArgumentException title was empty or blank.
   * @throws IllegalArgumentException id was empty or blank.
   * @author Pétur
   */
  Option(final String title, final String id) {
    Validate.notBlank(title);
    if (id != null)
      Validate.notBlank(id);

    this.title = title;
    this.id = (id == null) ? "" : id;
  }

  /**
   * Get this options title.
   *
   * @return this options title.
   * @author Pétur
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get this options id.
   * The id can be used to uniquely identify a option from a set of options belonging to the same question.
   *
   * @return this options id.
   * @author Pétur
   */
  public String getId() {
    return id;
  }

}
