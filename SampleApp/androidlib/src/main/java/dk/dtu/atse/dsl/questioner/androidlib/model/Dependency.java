package dk.dtu.atse.dsl.questioner.androidlib.model;

import org.apache.commons.lang3.Validate;

/**
 * A dependency connects two questions: a source and a destination.
 * It is used to control whether the source dk.dtu.atse.dsl.questioner.question should be asked, depending on a previous answered destination dk.dtu.atse.dsl.questioner.question.
 * A dk.dtu.atse.dsl.questioner.question.part.value is set on the destination dk.dtu.atse.dsl.questioner.question.
 * A conditional is used to compare the dk.dtu.atse.dsl.questioner.question.part.value with the destination questions answer.
 *
 */
class Dependency {
  private final Question source;
  private final Question destination;
  private final String value;
  private final Conditional conditional;

  /**
   * Get the source.
   *
   * @return source question.
   */
  Question getSource() {
    return source;
  }

  /**
   * Get destination.
   *
   * @return destination question.
   */
  Question getDestination() {
    return destination;
  }

  /**
   * Get value.
   *
   * @return value used when evaluating the dependency between source and destination.
   */
  String getValue() {
    return value;
  }

  /**
   * Evaluate this dependency.
   *
   * @return true if value holds with relation to the conditional on the destination; false otherwise or if the desination question has not been answered.
   * @author Pétur
   */
  boolean isSatisfied() {
    if (destination.hasBeenAnswered() == false)
      return false;

    /** An equal dependency does not make use of the source value. */
    if (conditional == Conditional.EQUAL)
      return destination.hasAnswer(value);

    // TODO How should we compare against a question which allows multiple answers to be provided? Currently we only compare against the first answer and ignore the others.
    return conditional.compare(destination.getAnswers().get(0).getValue(), value, destination.getExpectedAnswerType());
  }

  /**
   * Class Constructor.
   *
   * @param source the question which will not be asked unless this dependency is satisfied.
   * @param destination the question whos answer (if any) will be compared.
   * @param conditional the comparison to be performed.
   * @param value the value to be compared.
   * @throws IllegalArgumentException if any of the arguments are null.
   * @throws IllegalArgumentException if value is not of a valid answer type for destination.
   * @author Pétur
   */
    Dependency(final Question source,
                    final Question destination,
                    final Conditional conditional,
                    final String value) {
    Validate.notNull(source);
    Validate.notNull(destination);
    Validate.notNull(conditional);
    Validate.notNull(value);
    Validate.isTrue(destination.getExpectedAnswerType().isValid(value),
                    "Can not compare " + value + " against a question with answer of type " + destination.getExpectedAnswerType().toString());

    this.source = source;
    this.destination = destination;
    this.conditional = conditional;
    this.value = value;
  }

}
