package dk.dtu.atse.dsl.questioner.androidlib;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import dk.dtu.atse.dsl.questioner.androidlib.model.*;
import org.apache.commons.lang3.Validate;

import java.util.HashSet;
import java.util.Set;


/**
 * Activity that shows question from a given survey provider.
 * @author Jesper
 */
public class SurveyActivity extends Activity implements Interrogator {
  private static final String TAG = "SurveyActivity";

  public static final String EXTRA_SURVEY_PROVIDER_KEY =
          "dk.dtu.atse.dsl.questioner.androidlib.extraSurveyProviderKey";

  SurveyProvider surveyProvider;
  Question currentQuestion;
  Set<String> answers;

  TextView questionTextView;
  LinearLayout answerLayout;
  Button nextQuestionButton;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    surveyProvider = retrieveSurveyProvider();

    setContentView(R.layout.activity_survey);

    questionTextView = (TextView)findViewById(R.id.question_textview);
    answerLayout = (LinearLayout)findViewById(R.id.answer_layout);
    nextQuestionButton = (Button)findViewById(R.id.next_question_button);
    nextQuestionButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          Set<String> answers = currentQuestion.acceptResponder(SurveyActivity.this);
          surveyProvider.answer(currentQuestion, answers);
          currentQuestion = askNextQuestion();
        } catch (IllegalArgumentException ex) { //Answer does not have an appropriate format
          Toast.makeText(SurveyActivity.this,
              "Invalid response: " + ex.getLocalizedMessage(),
              Toast.LENGTH_SHORT).show();
        }
      }
    });

    currentQuestion = askNextQuestion();
  }

  @Override
  public void ask(Question question) {
    Log.e(TAG, "Could not handle question of type: " + question.getClass().getName());
  }

  @Override
  public void ask(Open question) {
    EditText answerEditText = new EditText(answerLayout.getContext());
    answerEditText.setId(R.id.open_answer_edittext);
    answerLayout.addView(answerEditText);
  }

  @Override
  public void ask(Closed question) {
    final LinearLayout buttonLayout;
    if (question.getMaxAnswers() == 1) {
      buttonLayout = new RadioGroup(answerLayout.getContext());
      answerLayout.addView(buttonLayout);
    } else {
      buttonLayout = answerLayout;
    }

    for (Option option : question.getOptions()) {
      CompoundButton newOptionButton;

      if (question.getMaxAnswers() == 1) {
        newOptionButton = new RadioButton(buttonLayout.getContext());
      } else {
        newOptionButton = new CheckBox(buttonLayout.getContext());
      }

      setupOptionButton(option.getTitle(), newOptionButton);

      buttonLayout.addView(newOptionButton);
    }
  }

  @Override
  public Set<String> answer(Question question) {
    Log.d(TAG, "Could not handle question of type: " + question.getClass().getName());
    return null;
  }

  @Override
  public Set<String> answer(Open question) {
    answers.clear();

    EditText answerEditText = (EditText)findViewById(R.id.open_answer_edittext);
    Validate.notNull(answerEditText, "Could not access open_answer_edittext resource.");

    answers.add(answerEditText.getText().toString());

    return answers;
  }

  @Override
  public Set<String> answer(Closed question) {
    //Check that number of answers is valid.
    if (answers.size() < question.getMinAnswers()) {
      int minAnswers = question.getMinAnswers();
      throw new IllegalArgumentException("Must provide at least " + minAnswers
          + " answer" + ((1 < minAnswers) ? "s." : "."));
    }
    else if (question.getMaxAnswers() < answers.size()) {
      int maxAnswers = question.getMaxAnswers();
      throw new IllegalArgumentException("Must provide at most " + maxAnswers
          + " answer" + ((1 < maxAnswers) ? "s." : "."));
    }

    return answers;
  }

  /**
   * Get the survey provider via a key retrieved from
   * the launching intent.
   * @return The survey provider.
   */
  private SurveyProvider retrieveSurveyProvider () {
    String surveyProviderTitle = getIntent().getStringExtra(EXTRA_SURVEY_PROVIDER_KEY);
    if (surveyProviderTitle == null) {
      throw new IllegalArgumentException("Intent contained no survey title.");
    }

    try {
      return SurveyProviderManager.getSurveyProvider(surveyProviderTitle);
    } catch (SurveyProviderManager.NoSuchKeyException e) {
      e.printStackTrace();
      finish();
    }

    return null;
  }

  /**
   * Gets a new question from the survey provider, updates
   * the interface, and empties the answer set.
   * @return The next question to be asked.
   */
  private Question askNextQuestion() {
    Question newQuestion = retrieveNextQuestion();

    if (newQuestion == null) {
      return null;
    }

    questionTextView.setText(newQuestion.getTitle());
    answerLayout.removeAllViews();

    newQuestion.acceptPresenter(this);
    answers = new HashSet<String>();

    return newQuestion;
  }

  /**
   * Retrieves the next question from the survey provider.
   * If no question is available from the survey provider,
   * the activity is exited.
   * @return The question retrieved from the survey provider.
   */
  private Question retrieveNextQuestion() {
    Validate.notNull(surveyProvider, "SurveyProvider is null.");
    try {
      return surveyProvider.getNextQuestion();
    } catch (SurveyProvider.NotAvailableException e) {
      Log.d(TAG, "Survey provider had no available questions.");
      finish();
    }

    return null;
  }

  /**
   * Sets the attributes of the option button for a closed
   * question. This includes the text and the CheckChangeListener.
   * @param optionTitle The text to be applied to the button.
   * @param newOptionButton Button to be set up.
   */
  private void setupOptionButton(String optionTitle, CompoundButton newOptionButton) {
    newOptionButton.setText(optionTitle);
    newOptionButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String answerText = buttonView.getText().toString();
        if (isChecked) {
          answers.add(answerText);
        }
        else {
          answers.remove(answerText);
        }
      }
    });
  }
}
