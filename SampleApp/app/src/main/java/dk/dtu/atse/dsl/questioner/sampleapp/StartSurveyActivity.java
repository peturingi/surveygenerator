package dk.dtu.atse.dsl.questioner.sampleapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Date;

import dk.dtu.atse.dsl.questioner.androidlib.QuestionAvailabilityChangeListener;
import dk.dtu.atse.dsl.questioner.androidlib.SurveyActivity;
import dk.dtu.atse.dsl.questioner.androidlib.SurveyProviderManager;
import dk.dtu.atse.dsl.questioner.androidlib.model.SurveyProvider;

/**
 * An activity exemplifying how androidlib's SurveyActivity and
 * supporting components can be used.
 * @author Jesper
 */
public class StartSurveyActivity extends Activity implements QuestionAvailabilityChangeListener {

  private static String KEY_SURVEY_TITLE = "surveyTitleKey";

  private SurveyProvider surveyProvider;

  private Button startButton;
  private Button viewResultsButton;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_start);

    // Build or retrieve the survey provider, using the
    // static SurveyProviderManager.
    try {
      if (savedInstanceState == null ||
          savedInstanceState.getString(KEY_SURVEY_TITLE) == null) {
        surveyProvider = SurveyProviderManager.buildSurveyProvider(null);
      } else {
        String surveyProviderName = savedInstanceState.getString(KEY_SURVEY_TITLE);
        surveyProvider = SurveyProviderManager.getSurveyProvider(surveyProviderName);
      }
      surveyProvider.setQuestionAvailabilityChangeListener(this);
    }
    catch (SurveyProviderManager.KeyInUseException e) {
      e.printStackTrace();
    }
    catch (SurveyProviderManager.NoSuchKeyException e) {
      e.printStackTrace();
    }

    // Set up the "Start survey" button to start the SurveyActivity,
    // adding the survey title to the intent.
    startButton = (Button)findViewById(R.id.start_button);
    startButton.setEnabled(surveyProvider.hasNewQuestion());
    startButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(StartSurveyActivity.this, SurveyActivity.class);
        i.putExtra(SurveyActivity.EXTRA_SURVEY_PROVIDER_KEY, surveyProvider.getSurveyTitle());
        startActivity(i);
      }
    });

    viewResultsButton = (Button)findViewById(R.id.view_results_button);
    viewResultsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent i = new Intent(StartSurveyActivity.this, ViewResultsActivity.class);
        i.putExtra(ViewResultsActivity.KEY_SURVEY_JSON, surveyProvider.getSurveyAsJson(new Date(0), true));
        startActivity(i);
      }
    });
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    // Save the title of the survey to the Bundle, so it can be
    // retrieved later.
    outState.putString(KEY_SURVEY_TITLE, surveyProvider.getSurveyTitle());
  }

  @Override
  public void questionAvailabilityChanged(final boolean questionAvailable) {
    // If question availability changes, change the state of the
    // "Start survey" button accordingly
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        startButton.setEnabled(questionAvailable);
      }
    });
  }
}
