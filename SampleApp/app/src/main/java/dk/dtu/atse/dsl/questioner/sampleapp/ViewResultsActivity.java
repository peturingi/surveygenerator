package dk.dtu.atse.dsl.questioner.sampleapp;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import dk.dtu.atse.dsl.questioner.androidlib.SurveyProviderManager;

/**
 * Activity showing the JSON representation
 * of the survey answers.
 * @author Jesper
 */
public class ViewResultsActivity extends Activity {

    public static String KEY_SURVEY_JSON = "dk.dtu.atse.dsl.questioner.sampleapp.surveyJsonKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        TextView resultTextView = (TextView)findViewById(R.id.resultTextView);
        resultTextView.setMovementMethod(new ScrollingMovementMethod());
        resultTextView.setText(getIntent().getStringExtra(KEY_SURVEY_JSON));
    }
}
